---?image=assets/img/_mandelbrot01.png
# [Manage Chaos](https://gitpitch.com/kommitment/verticals/master?grs=bitbucket&p=p-intro)
<!-- .element: style="font-size: 5em; color: orange"-->
Johannes Mainusch
<!-- .element: style="font-variant: smallcaps; color: orange"-->
Note:
![](assets/img/_mandelbrot01.png)

+++?image=assets/img/abstract.png
Note:
![](assets/img/abstract.png)



---?image=assets/img/change-otto01.png
Note:
![](../assets/img/change-otto01.png)

+++?image=assets/img/change-otto02.png
Note:
![](../assets/img/change-otto02.png)

+++?image=assets/img/change-otto03.png
Note:
![](../assets/img/change-otto03.png)

+++?image=assets/img/change-otto04.png
Note:
![](../assets/img/change-otto04.png)

+++?image=assets/img/change-otto05.png
Note:
![](../assets/img/change-otto05.png)

+++
<!-- .slide: data-background="#EBD5D6" style="text-align: left; font-size: 0.6em;"-->

# Lernziel I
# Das ideale Team
- unabhängig, share nothing (but beer and knowledge)
- kann den Erfolg seiner Arbeit selber messen 
- ist flexibel bei Anforderungsänderungen
- Das Produkt ist verstehbar
- das Team lernt und verbessert sich ständig
	- fachlich: in seiner Domäne
	- technisch: in seinem Tech-Stack
	- methodisch: in seiner Kommunikation

+++
<!-- .slide: data-background="#EBD5D6" style="text-align: left; font-size: 0.6em;" -->
# Lernziel II
# vertikale Organisationen
- Teamaufgaben sind für Laien verständlich
- Änderungswünsche sind daher einfach auf Teams zuzuordnen
- fachliche Schnitte entlang der Wertschöpfungskette
    (entlang von Services, Produkten, der user-journey)
- Spezialisierung im Management: bspw. 4-Dimensionen
- Restabhängigkeiten werden in diesen 
	Dimensionen behandelt (Gilden, CoPs, ...)
- Iterativ und agil --> KVP
- Die Organsation liefert exzellente Produkte/Services
- Personalverantwortung macht kein Micromanagement

Note:
Warum: um Chaos zu vermeiden. 
Frage: Hilft es auch das Chaos in den Griff zu bekommen?



---
<!-- .slide: style="text-align: left; font-size: 0.6em;"-->
# Method
+++
![](assets/img/V-Model-dj.jpg)
+++
![](assets/img/stillePostApfelZoom1.jpg)
+++
![](assets/img/stillePostApfelZoom2.jpg)
+++
![](assets/img/stillePostPonyZoom1.jpg)
+++
![](assets/img/stillePostPonyZoom2.jpg)
+++
![](assets/img/stillePostPonyZoom3.jpg)
+++
![](assets/img/stillePostPonyZoom4.jpg)
+++
![](assets/img/stillePostPonyZoom5.jpg)
+++
![](assets/img/stillePostPonyZoom6.jpg)
+++
<!-- .slide: style="text-align: left; font-size: 0.6em;"-->
# Agile
- stop telephone games
- start cross-functional work
- delegate decision making 
   to the working level
- excellent communication 

![](assets/img/_4D-OrganisationTeam-mini.png)





---
<!-- .slide: style="text-align: left; font-size: 0.6em;"-->
# Architecture
+++
![](assets/img/_prora.jpg)
+++?video=https://drive.google.com/file/d/0B1n0MV2Fqmdcem1PN0s0V0pScDg/view?usp=sharing
[https://drive.google.com/file/d/0B1n0MV2Fqmdcem1PN0s0V0pScDg/view?usp=sharing](https://drive.google.com/file/d/0B1n0MV2Fqmdcem1PN0s0V0pScDg/view?usp=sharing)
+++
[https://en.wikipedia.org/wiki/Conway%27s_law](https://en.wikipedia.org/wiki/Conway%27s_law)
+++?image=assets/img/conwayWolter.png
+++?image=assets/img/_swArchitecture.jpg
+++?image=assets/img/architecture03.png
+++?image=assets/img/architecture04.png
+++?image=assets/img/architecture05.png
+++?image=assets/img/architecture06.png



---
<!-- .slide: style="text-align: left; font-size: 0.6em;"-->
# Organization
+++?image=assets/img/orga01.png
+++?image=assets/img/orga02.png
+++?image=assets/img/_relationships2.png
+++?image=assets/img/_relationships3.png
+++?image=assets/img/_relationships4.png
+++?image=assets/img/_relationships11.png
+++?image=assets/img/_relationships44.png
+++?image=assets/img/orga05.png
+++?image=assets/img/orga06.png
+++?image=assets/img/orga07.png





---
<!-- .slide: style="text-align: left; font-size: 0.6em;"-->
# Product
+++?image=assets/img/product01.png
+++?image=assets/img/product02.png
+++?image=assets/img/product03.png
+++?image=assets/img/product04.png
+++?image=assets/img/product05.png
+++?image=assets/img/product06.png
+++?image=assets/img/product07.png
+++?image=assets/img/product08.png
+++?image=assets/img/product09.png
+++?image=assets/img/product10.png




---
<!-- .slide: style="text-align: left; font-size: 0.6em;"-->
# Portfolio
Now here I just show some pictures...



---
<!-- .slide: style="text-align: left; font-size: 0.6em;"-->
# Fazit

+++?include=/p-verticals/PITCHME.md
<!-- .slide: style="color: orange"-->

+++?image=assets/img/_mandelbrot01.png
# Fragen?
<!-- .element: style="font-size: 6em; color: orange"-->
	
+++
<!-- .slide: style="text-align: left; font-size: 0.6em;"-->
# Related stuff

- [Ralf Wirdemann, Johannes Mainusch, Scrum mit User Stories, Kapitel 12 "SCRUM@OTTO"](http://www.hanser-fachbuch.de/buch/Scrum+mit+User+Stories/9783446450523)
<!-- .element: style="color: white;" -->
- [Dr. John Kotter, Accelerate! The Evolution of the 21st Century Organization](https://www.youtube.com/watch?v=Pc7EVXnF2aI)
<!-- .element: style="color: white;" -->
- [Johannes Mainusch, "otto.de – wie die Titanic den Eisberg verfehlte"](https://www.heise.de/developer/artikel/Johannes-Mainusch-otto-de-wie-die-Titanic-den-Eisberg-verfehlte-3491223.html)
<!-- .element: style="color: white;" -->

+++?image=assets/img/kommitment.png



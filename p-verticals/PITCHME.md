<!-- .slide: style="text-align: left; font-size: 0.6em;"-->

# Ziel 1
# Das ideale Team
- unabhängig, share nothing (but beer and knowledge)
- kann den Erfolg seiner Arbeit selber messen
- ist flexibel bei Anforderungsänderungen
- Das Produkt ist verstehbar
- das Team lernt und verbessert sich ständig
	- fachlich: in seiner Domäne
	- technisch: in seinem Tech-Stack
	- methodisch: in seiner Kommunikation

+++
<!-- .slide: style="text-align: left; font-size: 0.6em;" -->
# Ziel 2
# Die ideale Organisationen
- Die Organisation liefert exzellente Produkte/Services
- ist effizient
- ist innovativ/effektiv


Note:
Warum: um Chaos zu vermeiden.
Frage: Hilft es auch das Chaos in den Griff zu bekommen?

+++
<!-- .slide: style="text-align: left; font-size: 0.6em;" -->
# HOW
<!-- .element: style="font-size: 5em; "-->

+++?image=assets/img/vertikaleKomponenten.png
<!-- .slide: data-background="#EBD5D6" style="text-align: left; font-size: 0.6em;" -->
	vertical components
<!-- .element: class="fragment fade-out" style="font-size: 2em; color: #ff7700;" -->
Note:
Vertikale Komponenten, d.h. die teilen sich nix. Schnittstellen über REST. Lose Kopplung im Frontend über Integration im Frontend (oder in  einem Page-Asseembly-Proxy).


+++?image=assets/img/vertikaleTeams.png
<!-- .slide: data-background="#EBD5D6" style="text-align: left; font-size: 0.6em;" -->
	# vertical teams
	- Teamaufgaben sind für Laien verständlich
	- Änderungswünsche sind einfach auf Teams zuzuordnen
	- fachliche Schnitte entlang der Wertschöpfungskette
    	(entlang von Services, Produkten, der user-journey)
	- Iterativ und agil --> KVP
<!-- .element: class="fragment fade-out" style="font-size: 1em; color: #ff7700;" -->

+++?image=assets/img/_4D-Organisation.jpg
<!-- .slide: data-background="#EBD5D6" style="text-align: left; font-size: 0.6em;" -->
	4D-Organization
	- Spezialisierung im Management: bspw. 4-Dimensionen
	- Restabhängigkeiten werden in diesen
	  Dimensionen behandelt (Gilden, CoPs, ...)
	- Die Organsation liefert exzellente Produkte/Services
	- Personalverantwortung macht kein Micromanagement
<!-- .element: class="fragment fade-out" style="font-size: 1em; color: #ff7700;" -->

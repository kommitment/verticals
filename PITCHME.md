
<!-- .slide: data-background="#ffffff"-->
# 大理石 <br>輸送
<!-- .element: style="font-size: 5em; color: #aa2332"-->

[Dairiseki-Yusō](https://gitpitch.com/kommitment/verticals/master?grs=bitbucket)
<!-- .element: style="font-variant: smallcaps; color: #aa2332"-->
+++?image=assets/img/abstract.png
Note:
![](assets/img/abstract.png)

---?include=/p-verticals/PITCHME.md




---
<!-- .slide: data-background="#D7ACAE" style="text-align: left; font-size: 0.6em;" -->
# Spiel
Zwei "Firmen" mit je vier(+) Teams und Management bauen Murmelbeförderungsanlagen (Dairiseki-Yusō). Ziel des Spieles ist es, mit der eigenen Firma im Vergleich zum Wettbewerb zu bestehen und nach vier Iterationen die Kunden mit einem Prototyp derart zu überzeugen, dass der Kunde mit der eigenen Firma weiter arbeiten wird.

+++
# Rollen
<!-- .slide: data-background="#D7ACAE" style="text-align: left; font-size: 0.6em;" -->
- Beobachter
- Kunde
- Manager (Firma I und Firma II)
- 4 Fertigungsteams (Firma I und Firma II)
	- Team Einlauf
	- Team Strecke
	- Team Zählwerk
	- Team Auslauf
- Jedes Team wählt einen Agilist, Architekten und Produkti 



+++
# Rolle Beobachter 
<!-- .slide: data-background="#D7ACAE" style="text-align: left; font-size: 0.6em;" -->
(2 Personen)

Aufgabe der Beobachter ist es, alles zu protokollieren und zu dokumentieren (Bild, Ton, Video) was ihnen auffällt. Am Ende des Spiels berichten die Beobachter:
-  Was ihnen auffiel
-  was gut gelaufen ist
-  was mies war
-  wie das Spiel verbessert werden kann

+++
# Rolle Kunde
<!-- .slide: data-background="#D7ACAE" style="text-align: left; font-size: 0.6em;" -->
(2 Personen)

Der Kunde hat ein altes Dairiseki-Yusō, das Kugeln bestimmter Größe von 1,5m Höhe schnell auf 0m Höhe befördert. Nun hat sich die Wirtschaftslage des Kunden geändert. Er könnte besseres Business machen, wenn er Kugeln variabel mal in 1,5m Höhe, aber auch mal höher bis zu 2m Höhe in sein Dairiseki-Yusō tun kann. 

Auch hätte er gern die Möglichkeit, dass die Kugeln variabel einstellbar zwischen 0,5 und 0m Höhe herauskommen. Weiterhin hat der Kunde festgestellt, dass es gut für das Business wäre, wenn das Dairiseki-Yusō die Kugeln möglichst über eine weite Strecke führt. Und zum internen Controlling soll das Dairiseki-Yusō einen möglichst automatischen Zähler enthalten.

Der Kunde hat schon zwei Firmen, die Firma 
ラウンド (Raundo) und die Firma 広場 (Hiroba) gefunden, die als Spezialisten für Dairiseki-Yusōs gelten. Er beauftragt beide, einen Prototypen zu bauen, damit er sich dann entscheiden kann, wer den Zuschlag bekommt. In regelmäßigen Intervallen besucht der Kunde die beiden Firman und macht sich Notizen, um am Ende eine qualifizierte Entscheidfung treffen zu können.

Der Kunde bekommen einen Zollstock(Klappmeter)

+++
<!-- .slide: data-background="#D7ACAE" style="text-align: left; font-size: 0.6em;" color: white;-->
# Rolle Manager 
(2, je Firma einen)

Der Manager ist für den Erfolg der Firma in der Zukunft verantwortlich. Es soll der Firma wirtschaftlich gut gehen. Außerdem sollen die Teams in der Firma gut arbeiten können.

Manager bekommen als Arbeitematerial einen Klappmeter (Zollstock).

+++ 

<!-- .slide: data-background="#D7ACAE" style="text-align: left; font-size: 0.6em; color: white;"-->
# Rolle Team 
(8 Teams, 4 je Firma)

Je Firma vier Teams mit den Namen Einlauf, Strecke, Zählwerk, Auslauf. Die Aufgaben der Teams sind:
-  Einlauf: nimmt Dinge zwischen 1,5m und 2m Höhe entgegen
-  Strecke: überbrückt eine möglichst weite Strecke
-  Zählwerk: zählt was durchgeht
-  Auslauf: Liefert in einer variablen Höhe von 0,5m-0m ab.

Das Team baut das Produkt. Die folgenden Rollen werden aus dem Team gewählt (Produkti, Architekt und Agilist).

+++
# Rolle Produkti
<!-- .slide: data-background="#D7ACAE" style="text-align: left; font-size: 0.6em; color: white;"-->
Der/die ideale Produkti weiss alles über Kundenanforderungen und den Markt. 

+++
<!-- .slide: data-background="#D7ACAE" style="text-align: left; font-size: 0.6em;" color: white;-->
# Rolle Architekt
Der/die Architekt/in verantwortet den technischen Bau und die Schnittstellen zu den umgebenden Teams.

+++
<!-- .slide: data-background="#D7ACAE" style="text-align: left; font-size: 0.6em;" color: white;-->
# Rolle Agilist
<!-- .slide: data-background="#C38286" style="text-align: left; font-size: 0.6em;" color: white;-->
Der/die Agilist/in sorgt für gute Kommunikation, Reflektion und Lernfähigkeit im Team. Er/sie sorgt auch dafür, dass es pro Iteration eine Retrospektive gibt. 







---
# Die Firmen
<!-- .slide: data-background="#C38286" style="text-align: left; font-size: 0.6em;" color: white;-->

+++?image=assets/img/rundes.png
<!-- .slide: data-background="#C38286" style="font-size: 0.6em;" color: white;-->
# ラウンド 
Die Firma Raundo
Note:
![raundo](assets/img/rundes.png)
+++?image=assets/img/eckiges.png
<!-- .slide: data-background="#C38286" style="font-size: 0.6em;" color: white;-->
# 広場 

Die Firma Hiroba
Note:
![hiroba](assets/img/eckiges.png)



---
<!-- .slide: data-background="#AF595E" style="text-align: left; font-size: 0.6em;" color: white;-->
# Das Spiel

+++
<!-- .slide: data-background="#AF595E" style="text-align: left; font-size: 0.6em;" color: white;-->
# Ausgangssituation
Es gibt schon ein System. Es ist darauf optimiert, Kugeln aus unterschiedlichen Materialien mit möglichst hoher Verlässlichkeit aufzunnehemn und an Nutzer auszugeben. 

*Altsystem: Geschlossenes Rohr, dass in 1,5m Höher beginnt und in kurzer Entfernung am Boden ankommt* 

Aktuell muss bei jeder Änderung am System, dass ganze System in den Wartungsmodus. 

Wenn es ein Problem mit dem System gibt, wird oben ein "Putz-Job" hineingeschoben und dann ausgemessen, an welcher Stelle das Problem wohl liegt. Von unten geht es nicht, weil der "Putz-Job" nicht den Berg hochkommt.

+++
<!-- .slide: data-background="#AF595E" style="text-align: left; font-size: 0.6em;" color: white;-->
# Anforderungen
Um im Markt agiler mitmischen zu können, beschließt das Managent, dass das alte System abgelöst werden soll.
Wichtigste Anforderung aus Sicht des Managements/Kunden: 
1. in Zukunft muss man von außen sehen können, wenn es ein Problem im System gibt.
2. Außerdem, soll das neue System nach Möglichkeit ohne längere Wartungspausen arbeiten können.
3. Die Kugeln müssen möglichst lange durch das System rollen (Umdrehungen machen) um den Mehrwert zu maximieren.
4. Aus Wartungsgründen soll das neue System aus den vier unabhängigen Teilstücken "Einlauf", Strecke", Zählwerk" und "Auslauf" bestehen.

**Ziel: #fachliche Schnitte entlang der Wertschöpfungskette**

+++
<!-- .slide: data-background="#AF595E" style="text-align: left; font-size: 0.6em;" color: white;-->
# Vorbereitung
- 2 Beobachter
- 2 Vertreter des Kunden
- Auditorium in Firmen teilen: ラウンド (Raundo) und 広場 (Hiroba)
- je Firma 2 Manager
- je Firma 4 Teams à ca. 7 Personen
	- jedes Team wählt den Produkti, Agilisten und Architekten	 

Die Produktis erstellen in Abstimmung mit dem Management eine Product-Vision und ein Product-Backlog.

**Ziel: #Personalverantwortung macht kein Micromanagement**

Die Agilisten legen fest, wie die Teams zusammen arbeiten.

**Ziel: #Spezialisierung im Management: bspw. 4-Dimensionen**

Die Architekten treffen sich in einer Gilde/CoP und verabschieden gemeinsam einen Bauplan. 

**Ziel: #Restabhängigkeiten werden in diesen Dimensionen behandelt (Gilden, CoPs)**

Die Teams erstellen ihre Team-Backlogs. 

**Ziel: #Das Produkt ist verstehbar**

+++
<!-- .slide: data-background="#AF595E" style="text-align: left; font-size: 0.6em;" color: white;-->
# Sprint 

Ein Sprint dauert 45 Minuten, dann gibt es 15min Pause.
Ein Sprint umfasst folgende Teile:
- 10min Retro: Das Team überlegt, wie es in diesem Sprint besser werden kann.
- 20min Bauphase (Die Teams bauen ihre Teilstrecke). Weiterhin treffen sich in der Bauphase die COPs/Gilden...
- 10min Deployment. Die Teams bauen ihre Teilstücke zusammen.
- 5min Review: Der Kunde kommt vorbei und guckt wie es läuft und testet das Ergebnis und bildet sich seine Meinung.
- 15min Pause


+++
<!-- .slide: data-background="#AF595E" style="text-align: left; font-size: 0.6em;" color: white;-->
# Agenda

- 10:00 - 11:00: Vortrag zum Thema Vertikalisierung
- 11:00 - 11:30: Spielregeln erläutern
- 11:30 - 12:00: Prototyp Phase, Sprint 0, CoPs
- 12:00 - 13:00: erster Sprint (Kunde kommt um 12:40)
- 13:00 - 13:30: Mittagspause
- 13:30 - 14:30: zweiter Sprint (Kunde kommt um 14:10)
- 14:30 - 15:00: Zwischenpräsentation
- 15:00 - 16:00: dritter Sprint (Kunde kommt um 15:40)
- 16:00 - 16:20: Feedback der Kunden / Wahl der Gewinnerfirma
- 16:20 - 16:40: Feedback der Beobachter
- 16:40 - 17:00: Gesamtretro / Fragen und Antworten
-         17:00: Ende

+++
<!-- .slide: data-background="#AF595E" style="text-align: left; font-size: 0.6em;" color: white;-->
# Sprint 1
Die Teams bauen die erste Version. 

**Ziel: #Iterativ und agil**

+++
<!-- .slide: data-background="#AF595E" style="text-align: left; font-size: 0.6em;" color: white;-->
# Sprint 2

Ein Team bekommt von einem Stakeholder die Anforderung das ganze "zukunftssicher" zu bauen. Sollte der PO nachfragen, bedeutet "zukunftssicher" lieber ein bisschen mehr Platz, falls in Zukunft mehr durch das System durch muss. 

**Ziel: #Team ist flexibel bei Anforderungsänderungen**

Ein weiteres Team bekommt die Anforderung, kaputte/unpassende Kuggeln die oben hineinkommen, wieder ausortieren zu können.

**Ziel: #Änderungswünsche sind einfach auf Teams zuzuordnen**

Die Teams bauen eine zweite Version (entweder komplett neu oder indem Version1 verbessert wird). 

**Ziel: #das Team lernt und verbessert sich ständig**
	**- fachlich: in seiner Domäne**
	**- technisch: in seinem Tech-Stack**
	**- methodisch: in seiner Kommunikation**

+++
<!-- .slide: data-background="#AF595E" style="text-align: left; font-size: 0.6em;" color: white;-->
# Sprint 3
Ein Endkunde/Konsument, merkt an, dass er eigentlich keine Glasmurmeln möchte, sondern Billiardkugeln....

**Ziel: #Die Organsation liefert exzellente Produkte/Services**





---
<!-- .slide: data-background="#9B3036" style="text-align: left; font-size: 0.6em;"-->
# Baumaterial
		30 Umzugskartons 
		2 Paketkleberoller 
		100 große Kabelbinder
		Viele Murmeln
		5m Abflussrohr für das Altsystem / 
		    Alternativ habe ich ein altes Stück Rohr
		30m Abflussrohr
		2 Puk-Sägen (
		1 Rolle braunes Packpapier (5m Rolle, 4 Stück)
		4 Rollen Tesa-Krepp
		2 Büro-Tacker und Munition 
		2 Rollen Paketschnur, 250 Blatt Papier,
		2 Schraubendreher
		3 Zollstock
<!-- .element: style="text-align: left; font-size: 1.0em;" -->
	
+++
<!-- .slide: data-background="#9B3036" style="text-align: left; font-size: 0.6em;"-->
# Related stuff

- [Ralf Wirdemann, Johannes Mainusch, Scrum mit User Stories, Kapitel 12 "SCRUM@OTTO"](http://www.hanser-fachbuch.de/buch/Scrum+mit+User+Stories/9783446450523)
<!-- .element: style="color: white;" -->
- [Dr. John Kotter, Accelerate! The Evolution of the 21st Century Organization](https://www.youtube.com/watch?v=Pc7EVXnF2aI)
<!-- .element: style="color: white;" -->
- [Johannes Mainusch, "otto.de – wie die Titanic den Eisberg verfehlte"](https://www.heise.de/developer/artikel/Johannes-Mainusch-otto-de-wie-die-Titanic-den-Eisberg-verfehlte-3491223.html)
<!-- .element: style="color: white;" -->
- [Johannes Mainusch, Manage Chaos](https://gitpitch.com/kommitment/verticals/master?grs=bitbucket&p=p-intro)
<!-- .element: style="color: white;" -->
- [2018-02-05 Workshop Doku](https://drive.google.com/drive/folders/0Bzr9vgG2NdI0U0tjWkszd1dUNWc?usp=sharing)
<!-- .element: style="color: white;" -->


+++
<!-- .slide: data-background="#9B3036" -->

[![kommitment][1]][2]
[1]: assets/img/kommitment.png
[2]: http://www.kommitment.biz
<!-- .element: style="border: 0px !important;" -->
